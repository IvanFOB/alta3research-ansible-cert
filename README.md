# Planet Express Ansible Playbook

This playbook utilizes the Planet Express hosts and variables defined in the training environment.

## Setup

Run the following script to set up the environment:

```bash
. px/scripts/full-setup.sh
```

## How to Run

Execute the playbook using the following command:

```bash
ansible-playbook alta3research-ansiblecert01.yml
```

## Verification

You can verify the results on the controller node with the following files:

- `file-data-zoidberg.txt`
- `test-back-fry.txt`
- `file-data-bender.txt`
- `test-back-zoidberg.txt`
- `file-data-farnsworth.txt`
- `test-back-bender.txt`
- `file-data-fry.txt`
- `test-back-farnsworth.txt`

Additionally, check the files on each host.

### Expected Output


```bash
student@bchd:~$ cat file-data-bender.txt 
My name is: bender
My ip address: 10.10.2.3
Today's date is: 2024-05-14
The time is: 22:55:39
student@bchd:~$ 
student@bchd:~$ cat test-back-bender.txt 
This is a text modified from bender
student@bchd:~$
```